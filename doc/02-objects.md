# Fuel Sdk - Objects


## Query Objects
#### QueryParam
Los valores de QueryRelation son:
* FUEL_INT_QUERY_RELATION_EQUAL it is equivalent to equal "="
* FUEL_INT_QUERY_RELATION_GROW it is equivalent to grow than ">"
* FUEL_INT_QUERY_RELATION_LESS it is equivalent to less than "<"
* const FUEL_INT_QUERY_RELATION_LIKE  only available for strings search that content this value.
 Equivalent to "LIKE"
```php
//QueryParam allow filter by Entity's attribute, and type filter. Its the basic Object.
$params[] = new QueryParam("nameField", QueryRelation::FUEL_INT_QUERY_RELATION_GROW, 'valueFieldWithSameDataType');

// It is possible that value be an array. For that please use FUEL_INT_QUERY_RELATION_EQUAL it works as "field IN (array)"
$params[] = new QueryParam("nameField", QueryRelation::FUEL_INT_QUERY_RELATION_EQUAL, 'valueFieldWithSameDataType');

//Method requestWilcardGet allow do request for single element and for list. 
$conn->requestWilcardGet("/usuarios/item", $params);
```
#### QueryOrder
```php
 #To sort results must use QueryOrder with the name of the field and the sense of the order
 $params[]  = new QueryOrder("fechaAlta", QueryOrder::FUEL_STR_ORDER_DESC);

 //Method requestWilcardGet allow do request for single element and for list. 
 $conn->requestWilcardGet("/usuarios/item", $params);
```

#### QueryPagination
```php
 //QueryPagination allow return specific page, and number results por page (page, maxResults).   
 $params[] = new QueryPagination(3,2);

 //Also its possible send only max number results by QueryParam
 //$params[] = new QueryParam("maxResults", QueryRelation::FUEL_INT_QUERY_RELATION_EQUAL, 2);

 //Method requestWilcardGet allow do request for single element and for list. 
 $conn->requestWilcardGet("/usuarios/item", $params);
```

#### QuerySearchCustom
```php
 //To search, SearchCustom must be used, which looks for a string to be contained in any of the pre-defined fields of the entity.
 $params[]  = new QuerySearchCustom("9999zzz");

 //Method requestWilcardGet allow do request for single element and for list. 
 $conn->requestWilcardGet("/usuarios/item", $params);
```

#### QueryPagination
```php
 //Get normalized object customized. Use QueryStyleResponse with name style for get structure.
 //Not mandatory but its recommended for optimize time response.
 $params[] = new \FuelSdk\Utils\QueryStyleResponse("mobile_list");

 //Method requestWilcardGet allow do request for single element and for list. 
 $conn->requestWilcardGet("/usuarios/item", $params);
```

## Objects available after Request
#### Response
Raw response from Fuel. Data can be an object or a list of objects
```json
{
	"data": {
		"id": 80
	},
	"error": "",
	"codeResponse": 1,
	"pagination": {
		"page": 1,
		"results": 20,
		"prevPage": null,
		"nextPage": 2,
		"numberPages": 2
	}
}
```
In this sdk the above response structure can be access by the next objects
```php
//Content all info about response recived. 
//The response object is saved even though an exception has been thrown. 
//This is useful for 409 response codes to retrieve the already existing object id.
$conn->getResponse();


#DATA
//Value of last response insede "data" key of json.
$conn->getResponse()->getData();

#PAGINATION
//On list request, with list of objects response. 
$conn->getResponse()->getPagination();
/*pagination is an object and have that attributes, are accesible by getters
    protected $page;
    protected $results;
    protected $prevPage;
    protected $nextPage;
    protected $numberPages;
 */
//For get all next pages can be used:
$iterations = $conn->getResponse()->getPagination()->getNumberPages();
while($iterations>1)
{
    $conn->requestNextPage();
    var_dump($conn->getResponse()->getData());
    $iterations--;
}
 
#OTHERS
//Get "error" key of json response. Usefull on ConnectionException (httpCode != 200)
$conn->getResponse()->getError();

//Get "httpCode" of response, usefull on ConnectionException (httpCode != 200)
$conn->getResponse()->getHttpCode();

//Get id code response internal in Fuel
$conn->getResponse()->getCodeResponse();

//Return raw response.
$conn->getResponse()->getOriginalResponse();

    
```



