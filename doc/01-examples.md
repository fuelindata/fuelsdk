# Fuel Sdk - How to Use


## Login with Email and Pass

```php
<?php

use FuelSdk\ConnectionEmailPass;
use FuelSdk\Connection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

//Create logger object. Use your favorite Handler. Example for file output. 
$logger = new Logger("Log Sdk");
$logger->pushHandler(new StreamHandler('./nameFile.log'));

// create connection
// You can debug the request by setting the fifth parameter to true
$conn = new ConnectionEmailPass("passUser","emailUser", $logger, Connection::FUEL_NAME_DEV, null, true);

```

## Login B2B with key and Pass

```php
<?php

use FuelSdk\ConnectionB2b;
use FuelSdk\Connection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

//Create logger object. Use your favorite Handler. Example for file output. 
$logger = new Logger("Log Sdk");
$logger->pushHandler(new StreamHandler('./nameFile.log'));

// create connection
// You can debug the request by setting the fifth parameter to true
$conn = new ConnectionB2b("passUser","key", $logger, Connection::FUEL_NAME_DEV, null, true);
//Id License/client
$conn->setLicenseId(126);
//Also set the user who makes the request
$conn->setUserId(1);

//Also set the user who makes the request using the old Maxterauto id
$conn->setUserIdOld(9012);

```

## Authenticated IP

```php
<?php

use FuelSdk\ConnectionIpAuthenticated;
use FuelSdk\Connection;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

//Id License/client
$licenseId = 1;

//Create logger object. Use your favorite Handler. Example for file output. 
$logger = new Logger("Log Sdk");
$logger->pushHandler(new StreamHandler('./nameFile.log'));

// create connection. On create try to retrieve client list. 
// You can debug the request by setting the fifth parameter to true
$conn = new ConnectionIpAuthenticated($licenseId, $logger, Connection::FUEL_NAME_DEV, null, true);
//Also set the user who makes the request
$conn->setUserId(1);

//Also set the user who makes the request using the old Maxterauto id
$conn->setUserIdOld(9012);


```
## Examples
#### Example GET single result
```php
#REQUEST GET OBJECT
$params[] = new QueryParam("id", QueryRelation::FUEL_INT_QUERY_RELATION_EQUAL, 1);

//Method requestWilcardGet allow do request for single element and for list. 
$conn->requestWilcardGet("/usuarios/item", $params);

```

#### Example GET list, with pagination
```php
#REQUEST GET LIST OBJECT [FILTERED]
$params = [];

//QueryParam allow filter by Entity's attribute, and type filter
$params[] = new QueryParam("fechaAlta", QueryRelation::FUEL_INT_QUERY_RELATION_GROW, '2019-08-12');

//QueryPagination allow return specific page, and number results por page (page, maxResults).   
$params[] = new QueryPagination(3,2);

 //Also its possible send only max number results by QueryParam
 //$params[] = new QueryParam("maxResults", QueryRelation::FUEL_INT_QUERY_RELATION_EQUAL, 2);
 
 //To sort results must use QueryOrder with the name of the field and the sense of the order
 $params[]  = new QueryOrder("fechaAlta", QueryOrder::FUEL_STR_ORDER_DESC);
         
 //To search, SearchCustom must be used, which looks for a string to be contained in any of the pre-defined fields of the entity.
 $params[]  = new QuerySearchCustom("9999zzz");
 
 //Get normalized object customized. Use QueryStyleResponse with name style for get structure.
 //Not mandatory but its recommended for optimize time response.
 $params[] = new \FuelSdk\Utils\QueryStyleResponse("mobile_list");

//Method requestWilcardGet allow do request for single element and for list. 
$conn->requestWilcardGet("/usuarios/list", $params);
var_dump(json_encode($conn->getResponse()->getData()));

$iterations = $conn->getResponse()->getPagination()->getNumberPages();
while($iterations>1)
{
    $conn->requestNextPage();
    var_dump($conn->getResponse()->getData());
    $iterations--;
}
```

#### Example POST
```php
#REQUEST POST OBJECT
$data = array(
            "sede"=> 2000,
            "tipoCliente"=>3,
            "tratamiento"=>1,
            "nombre"=> "Ernesto",
            "apellidos"=> "Adroher Cuevas",
            "fechaNacimiento"=> "1980-04-20",
            "usuarioOriginal"=> 1,
            "usuarioResponsable"=> 1,
            "procedencia"=>3,
            "concesionariosPermitidos"=> [999,268],
            "fechaAlta"=> "2020-03-09 17:02:50"
        );
 //data is sended in body as json inside "data"
        
$conn->requestWilcardPost("/terceros/create", $data);
//Trow an exception if httpcode != 200

//Set styleResponse it's possible with QueryParam
//$params[] = new QueryStyleResponse("mobile_list");
//$conn->requestWilcardPost("/terceros/create", $data, $params);

//Get Id new object
$responseData = $conn->getResponse()->getData();
if(isset($responseData['id']))
{
    $id = $responseData['id']
}

//Get object created
var_dump($conn->getResponse()->getData());

```

#### Example PUT
```php
$id = 1; //Id get by GET request item or list, or by previous POST request to create.
$data = array(
        "id" => $id,
        "nombre"=> "Carlos",
        "apellidos"=> "Santander Huelva",
);
 //data is sended in body as json inside "data"

$conn->requestWilcardPut("/terceros/update", $data);
 //Trow an exception if httpcode != 200 No update element
 
 //Set styleResponse it's possible with QueryParam
 //$params[] = new QueryStyleResponse("mobile_list");
 //$conn->requestWilcardPost("/terceros/update", $data, $params);
 
```

#### Example DELETE
```php
$id = 1; //Id get by GET request item or list, or by previous POST request to create.
$params[] = new QueryParam("id", QueryRelation::FUEL_INT_QUERY_RELATION_EQUAL, $id);

$conn->requestWilcardDelete("/terceros/delete", $params);   
//Trow an exception if httpcode != 200 No element deleted

 //Set styleResponse it's possible with QueryParam
 //$params[] = new QueryStyleResponse("mobile_list");
 //$conn->requestWilcardDelete("/terceros/delete", $params);
 
```

## HTTP CODES
- 200: OK
- 204: NOT FOUND. Element's Id not found for this license. Available on GET or DELETE for item or list. On Create or Update when related object's id not exists.
- 400: Insufficient parameters on request. Normally on PUT or POST request.
- 401: Invalid credentials, require refresh.
- 409: Already exists element. Usually on POST request.
- 500: Internal Server Error. Report to developers.




