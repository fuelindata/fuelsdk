# Fuel Sdk - Connection with Fuel 
[![Total Downloads](https://img.shields.io/packagist/dt/tilomotion/fuelsdk.svg)](https://packagist.org/packages/tilomotion/fuelsdk)
[![Latest Stable Version](https://img.shields.io/packagist/v/tilomotion/fuelsdk.svg)](https://packagist.org/packages/tilomotion/fuelsdk)

This library allow use Fuel's API with all kind of authentication.

## Installation

Install the latest version with..

```bash
$ composer require tilomotion/fuelsdk
```

## HTTP CODES
- 200: OK 
- 204: NOT FOUND. Element's Id not found for this license. Available on GET or DELETE for item or list. On Create or Update when related object's id not exists.
- 400: Insufficient parameters on request. Normally on PUT or POST request.
- 401: Invalid credentials, require refresh.
- 409: Already exists element. Usually on POST request.
- 412: Already exists on Dms or other external platform
- 416: Exceeded results obteined.
- 421: Fail login or conection with Dms or other external platform.
- 500: Internal Server Error. Report to developers.

## Documentation

- [Fuel's postman collection](https://tilo-my.sharepoint.com/personal/agago_tilomotion_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fagago%5Ftilomotion%5Fcom%2FDocuments%2FFuel%2FColeccion%20Postman%2FAPI%5FFUEL%5Fv200%2Epostman%5Fcollection%2Ejson&parent=%2Fpersonal%2Fagago%5Ftilomotion%5Fcom%2FDocuments%2FFuel%2FColeccion%20Postman&ct=1654507735296&or=Teams%2DHL&ga=1)
- [API Examples](doc/01-examples.md)
- [Glossary of Objects](doc/02-objects.md)

## New in 2.0.0
- Login B2B.
```php
$conn = new ConnectionB2b("pass", "key", $logger, "DEV");
```

## New in 1.0.8
- Continuous integration of bitbucket repository with packagist.
- Constants with the path of the services exposed in the Fuel API. Use class WebService.
```php
$params[]  = new QueryOrder("fechaAlta", QueryOrder::FUEL_STR_ORDER_DESC);
$conn->requestWilcardGet(WebService::PATH_USER_LIST, $params);
```

## New in 2.2.0
- Not sufix queryParam allowed by boolean parameter
- NotNull sufix queryParam allowed by boolean parameter
```php
$params[]  = new QueryParam("fechaAlta", QueryParam::FUEL_INT_QUERY_RELATION_IS_NULL, /* For apply negation  !=*/ true);
$conn->requestWilcardGet(WebService::PATH_USER_LIST, $params);
```
- Fix urlEncoded only in specified keys


## Third Party Packages
Third party handlers, formatters and processors are
[listed in the wiki](https://github.com/agagoenc/fuel-sdk/wiki/Third-Party-Packages). You
can also add your own there if you publish one.

## About

#### Requirements

- Fuel sdk works with  PHP 5.4 or above.

#### Submitting bugs and feature requests

Bugs and feature request are tracked on [Bitbucket](https://bitbucket.org/fuelindata/fuelsdk)

#### Framework Integrations

- Frameworks and libraries using [PSR-3](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md)
  can be used very easily with Monolog since it implements the interface.

#### Author

Alejandro Gago Encinas - <agago@tilomotion.com><br />
Carmelo Ramírez Robles - <cramirez@tilomotion.com><br />


#### License

FuelSdk is licensed under the MIT License - see the `LICENSE` file for details


