<?php


namespace FuelSdk;


abstract class WebService
{
    //Auth
    const PATH_AUTH_LOGIN = "/auth/login/user";
    const PATH_AUTH_LOGIN_B2B = "/b2bauth/login/user";
    const PATH_AUTH_REFRESH_B2B = "/b2bauth/token/refresh";
    const PATH_AUTH_REFRESH = "/auth/token/refresh";

    //Notificaciones
    const PATH_NOTIFICACION_TOKEN_ADD = "/notificacion/token";
    const PATH_NOTIFICACION_LOGOUT = "/notificacion/token";
    const PATH_NOTIFICACIONES_ITEM = "/notificaciones/item";
    const PATH_NOTIFICACIONES_LIST_USUARIO = "/notificaciones/list/usuario";
    const PATH_NOTIFICACIONES_UPDATE_LEIDA = "/notificaciones/update/leida";
    const PATH_NOTIFICACIONES_CREATE = "/notificaciones/create";

    //CRUD clientes
    const PATH_CLIENTE_LIST = "/clientes/list";

    //CRUD colores
    const PATH_COLORESVEHICULOS_ITEM = "/colores/vehiculos/item";
    const PATH_COLORESVEHICULOS_LIST = "/colores/vehiculos/list";
    const PATH_COLORESVEHICULOS_CREATE = "/colores/vehiculos/create";
    const PATH_COLORESVEHICULOS_UPDATE = "/colores/vehiculos/update";

    //CRUD combustibles
    const PATH_COMBUSTIBLES_LIST_ALL = "/combustibles/list/all";

    //CRUD concesionarios
    const PATH_CONCESIONARIO_GET = "/concesionarios/item";
    const PATH_CONCESIONARIOS_GET_ITEM_CUSTOM = "/concesionarios/itemcustom";
    const PATH_CONCESIONARIO_LIST = "/concesionarios/list";
    const PATH_CONCESIONARIOS_GET_CUSTOM = "/concesionarios/listcustom";
    const PATH_CONCESIONARIO_CREATE = "/concesionarios/create";
    const PATH_CONCESIONARIO_UPDATE = "/concesionarios/udpate";
    const PATH_CONCESIONARIO_DELETE = "/concesionarios/delete";

    //CRUD ConfiguracionCarteleria
    const PATH_CONFIGURACIONCARTELERIA_CREATE = "/configuracioncarteleria/create";
    const PATH_CONFIGURACIONCARTELERIA_UPDATE = "/configuracioncarteleria/update";
    const PATH_CONFIGURACIONCARTELERIA_LIST = "/configuracioncarteleria/list";
    const PATH_CONFIGURACIONCARTELERIA_ITEM = "/configuracioncarteleria/item";

    //Distintivo ambiental
    const PATH_DISTINTIVO_AMBIENTAL_LIST_ALL = "/distintivo_ambiental/list/all";

    //CRUD tasaciones documentacion
    const PATH_TASACIONES_DOCUMENTACION_ITEM = "/vehiculo/tasaciones/documentacion/item";
    const PATH_TASACIONES_DOCUMENTACION_LIST = "/vehiculo/tasaciones/documentacion/list/vehiculo";
    const PATH_TASACIONES_DOCUMENTACION_LIST_CONCESIONARIO = "/vehiculo/tasaciones/documentacion/tipos/list/concesionario";
    const PATH_TASACIONES_DOCUMENTACION_CREATE_VEHICULO = "/vehiculo/tasaciones/documentacion/createvehiculo";
    const PATH_TASACIONES_DOCUMENTACION_CREATE_CONTENT_VEHICULO = "/vehiculo/tasaciones/documentacion/createvehiculo/content";
    const PATH_TASACIONES_DOCUMENTACION_UPDATE = "/vehiculo/tasaciones/documentacion/update";
    const PATH_TASACIONES_DOCUMENTACION_UPDATE_CONTENT = "/vehiculo/tasaciones/documentacion/update/content";
    const PATH_TASACIONES_DOCUMENTACION_DELETE = "/vehiculo/tasaciones/documentacion/delete";

    //Estadisticas stock
    const PATH_ESTADISTICAS_STOCK_MOBILE = "/estadisticas/stock/mobile";

    //Formulario dinamico
    const PATH_FORMULARIO_DINAMICO = "/formulario/dinamico";
    const PATH_FORMULARIO_DINAMICO_SIMPLIFICADO = "/formulario/dinamico/simplificado";
    const PATH_FORMULARIO_DINAMICO_CARFAX = "/formulario/dinamico/carfax";

    //CRUD fotos encuadre
    const PATH_TASACIONES_FOTOS_ENCUADRE_ITEM = "/vehiculo/tasaciones/fotos/encuadre/item";
    const PATH_TASACIONES_FOTOS_ENCUADRE_LIST_CONCESIONARIO = "/vehiculo/tasaciones/fotos/encuadre/list/concesionario";
    const PATH_TASACIONES_FOTOS_ENCUADRE_LIST = "/vehiculo/tasaciones/fotos/encuadre/list";
    const PATH_TASACIONES_FOTOS_ENCUADRE_LIST_VN = "/vehiculo/tasaciones/fotos/encuadre/vn/list";
    const PATH_TASACIONES_FOTOS_ENCUADRE_LIST_VO = "/vehiculo/tasaciones/fotos/encuadre/vo/list";
    const PATH_TASACIONES_FOTOS_ENCUADRE_CREATE = "/vehiculo/tasaciones/fotos/encuadre/create";
    const PATH_TASACIONES_FOTOS_ENCUADRE_UPDATE = "/vehiculo/tasaciones/fotos/encuadre/update";

    //CRUD tasaciones fotos
    const PATH_TASACIONES_FOTOS_ITEM = "/vehiculo/tasaciones/fotos/item";
    const PATH_TASACIONES_FOTOS_LIST_VEHICULO = "/vehiculo/tasaciones/fotos/list/concesionario";
    const PATH_TASACIONES_FOTOS_CREATE_VEHICULO = "/vehiculo/tasaciones/fotos/createvehiculo";
    const PATH_TASACIONES_FOTOS_CREATE_CONTENT_VEHICULO = "/vehiculo/tasaciones/fotos/create/content";
    const PATH_TASACIONES_FOTOS_UPDATE = "/vehiculo/tasaciones/fotos/update";
    const PATH_TASACIONES_FOTOS_UPDATE_CONTENT = "/vehiculo/tasaciones/fotos/update/content";
    const PATH_TASACIONES_FOTOS_DELETE = "/vehiculo/tasaciones/fotos/delete";

    //CRUD fotos vehiculos
    const PATH_VEHICULOS_FOTOS_ITEM = "/vehiculo/fotos/item";
    const PATH_VEHICULOS_FOTOS_LIST_VEHICULO = "/vehiculo/fotos/list/concesionario";
    const PATH_VEHICULOS_FOTOS_CREATE_VEHICULO = "/vehiculo/fotos/createvehiculo";
    const PATH_VEHICULOS_FOTOS_CREATE_VEHICULO_CONTENT = "/vehiculo/fotos/createvehiculo/content";
    const PATH_VEHICULOS_FOTOS_UPDATE = "/vehiculo/fotos/update";
    const PATH_VEHICULOS_FOTOS_UPDATE_CONTENT = "/vehiculo/fotos/update/content";
    const PATH_VEHICULOS_FOTOS_DELETE = "/vehiculo/fotos/delete";
    const PATH_VEHICULOS_FOTOS_DELETE_IMPORT = "/vehiculo/fotos/delete/importador";

    //?
    const PATH_IMAGENES_VEHICULOS_UPLOAD = "/imagenes/vehiculos/upload";

    //CRUD marcas
    const PATH_MARCAS_ITEM = "/marcas/item";
    const PATH_MARCAS_LIST = "/marcas/list";
    const PATH_MARCAS_CREATE = "/marcas/create";
    const PATH_MARCAS_UPDATE = "/marcas/update";

    //CRUD modelos
    const PATH_MODELOS_ITEM = "/modelos/item";
    const PATH_MODELOS_LIST = "/modelos/list";
    const PATH_MODELOS_LIST_CUSTOM_MODELOS = "/modelos/listcustom";
    const PATH_MODELOS_CREATE = "/modelos/create";
    const PATH_MODELOS_UPDATE = "/modelos/update";

    //CRUD negociaciones
    const PATH_NEGOCIACIONES_SEARCH = "/negociaciones/search";
    const PATH_NEGOCIACIONES_ITEM = "/negociaciones/item";
    const PATH_NEGOCIACIONES_LIST = "/negociaciones/list";
    const PATH_NEGOCIACIONES_CREATE = "/negociaciones/create";
    const PATH_NEGOCIACIONES_UPDATE = "/negociaciones/update";

    //Procedencias
    const PATH_PROCEDENCIAS_LIST_ALL = "/procedencias/list/all";

    //Regiones
    const PATH_REGION1_GET_CUSTOM = "/region1/list";
    const PATH_REGION2_GET_CUSTOM = "/region2/list";
    const PATH_REGION3_GET_CUSTOM = "/region3/list";

    //Resultados ITV
    const PATH_RESULTADOSITV_LIST_ALL = "/itv/resultados/list/all";

    //Tapicerias
    const PATH_TAPICERIAS_LIST_ALL = "/tapicerias/list/all";

    //CRUD sedes
    const PATH_SEDES_SEARCH = "/sedes/item";
    const PATH_SEDES_GET = "/sedes/list";
    const PATH_SEDES_GET_CUSTOM = "/sedes/listcustom";
    const PATH_SEDES_CREATE = "/sedes/create";
    const PATH_SEDES_UPDATE = "/sedes/update";
    const PATH_SEDES_DELETE = "/sedes/delete";

    //CRUD tasaciones
    const PATH_TASACIONES_ITEM = "/tasaciones/item";
    const PATH_TASACIONES_LIST = "/tasaciones/list";
    const PATH_TASACIONES_LIST_COMERCIAL = "/tasaciones/list/comercial";
    const PATH_TASACIONES_LIST_TASADOR = "/tasaciones/list/tasador";
    const PATH_TASACIONES_CREATE_EMPTY = "/tasaciones/create/empty";
    const PATH_TASACIONES_UPDATE = "/tasaciones/update";
    const PATH_TASACIONES_DELETE = "/tasaciones/delete";
    const PATH_TASACIONES_UPDATE_ESTADO = "/tasaciones/update/estado";

    //CRUD terceros
    const PATH_TERCEROS_ITEM = "/terceros/item";
    const PATH_TERCEROS_SEARCH = "/terceros/search";
    const PATH_TERCEROS_LIST = "/terceros/list";
    const PATH_TERCEROS_LIST_FAVORITOS = "/terceros/list/favoritos";
    const PATH_TERCEROS_FAVORITO_ADD = "/terceros/favorito/add";
    const PATH_TERCEROS_FAVORITO_DELETE = "/terceros/favorito/delete";
    const PATH_TERCEROS_CREATE = "/terceros/create";
    const PATH_TERCEROS_UPDATE = "/terceros/update";
    const PATH_TERCEROS_DELETE = "/terceros/delete";

    // CRUD users
    const PATH_USER_GET = "/usuarios/item";
    const PATH_USER_SEARCH = "/usuarios/search";
    const PATH_USER_LIST = "/usuarios/list";
    const PATH_USER_CREATE = "/usuarios/create";
    const PATH_USER_UDPATE = "/usuarios/update";
    const PATH_USER_DELETE = "/usuarios/delete?id=4";

    //CRUD vehiculoTasaciones
    const PATH_VEHICULO_TASACIONES_SEARCH = "/vehiculo/tasaciones/item";
    const PATH_VEHICULO_TASACIONES_LIST = "/vehiculo/tasaciones/list";
    const PATH_VEHICULO_TASACIONES_CREATE = "/vehiculo/tasaciones/create";
    const PATH_VEHICULO_TASACIONES_UPDATE = "/vehiculo/tasaciones/update";
    const PATH_VEHICULO_TASACIONES_DELETE = "/vehiculo/tasaciones/delete";

    //CRUD stock vehiculos (vehiculos v2)
    const PATH_STOCK_VEHICULOS_ITEM = "/stock/vehiculos/item";
    const PATH_STOCK_VEHICULOS_LIST = "/stock/vehiculos/list";
    const PATH_STOCK_VEHICULOS_CREATE = "/stock/vehiculos/create";
    const PATH_STOCK_VEHICULOS_UPDATE = "/stock/vehiculos/update";
    const PATH_STOCK_VEHICULOS_DELETE = "/stock/vehiculos/delete";
    const PATH_STOCK_VEHICULOS_CARFAX = "/stock/vehiculos/carfax";

    //CRUD vehiculos v1 (deprecado no se pueden usar en sdk)
    /*
    const PATH_DEPRECATED_VEHICULOS_LIST                                     =    "/vehiculos/list";
    const PATH_DEPRECATED_VEHICULOS_DETAILS                                  =    "/vehiculos/details/{vehiculo}";
    const PATH_DEPRECATED_VEHICULOS_CREATE                                   =    "/vehiculos/create";
    const PATH_DEPRECATED_VEHICULOS_DELETE                                   =    "/vehiculos/delete/{vehiculo}";
    const PATH_DEPRECATED_VEHICULOS_UPDATE                                   =    "/vehiculos/update/{vehiculo}";
    const PATH_DEPRECATED_VEHICULOS_MATRICULA                                =    "/vehiculos/matricula";
    const PATH_DEPRECATED_VEHICULOS_BASTIDOR                                 =    "/vehiculos/bastidor";
    const PATH_DEPRECATED_VEHICULOS_EXISTS                                   =    "/vehiculos/exists";
    */

	//CRUD cobros
	const PATH_COBRO_ITEM = "/cobro/item";
}