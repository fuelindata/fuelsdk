<?php


namespace FuelSdk;


use FuelSdk\DTO\Generic\ResponseDTO;
use FuelSdk\Exception\ConnectionCodes;
use FuelSdk\Exception\ConnectionException;
use FuelSdk\Utils\QueryItem;
use Monolog\Logger;

abstract class Connection
{

    const FUEL_URL_DEV = "https://apidev.fuelindata.com";
    const FUEL_URL_TEST = "https://apitest.fuelindata.com";
    const FUEL_URL_TESTINT = "https://apitestint.fuelindata.com";
    const FUEL_URL_TEST_1 = "https://apitest1.fuelindata.com";
    const FUEL_URL_TEST_2 = "https://apitest2.fuelindata.com";
    const FUEL_URL_TEST_3 = "https://apitest3.fuelindata.com";
    const FUEL_URL_PROD = "https://api.fuelindata.com";
    const FUEL_URL_STAGING = "https://mx8-api.tlodev.com";
    const FUEL_URL_LOCAL = "http://localhost:8080";
    const FUEL_NAME_DEV = "DEV";
    const FUEL_NAME_TEST = "TEST";
    const FUEL_NAME_TESTINT = "TESTINT";
    const FUEL_NAME_TEST_1 = "TEST_1";
    const FUEL_NAME_TEST_2 = "TEST_2";
    const FUEL_NAME_TEST_3 = "TEST_3";
    const FUEL_NAME_PROD = "PROD";
    const FUEL_NAME_STAGING = "STAGING";
    const FUEL_NAME_LOCAL = "LOCAL";
    const FUEL_NAME_CUSTOM = "CUSTOM";
    const FUEL_URL_ENV_NAME = "FUEL_URL";
    const USER_AGENT_NAME = "Sdk Fuel 2.2.0";
    const SDK_VERSION = "v2.2.0";
    const ERROR_TRANSLATED_RESPONSE_VARNAME = 'errorToShow';
    //Iternal params
    protected $url;
    protected $environment;
    protected $fechaRefrescoLicencia;
    protected $logger;
    protected $request;
    /** @var ResponseDTO $response */
    protected $response;
    protected $httpcode;
    protected $lastUrlRequest;
    protected $customUrl;
    protected $errorToShow;
    protected $debug = false;

    /**
     * Connection constructor.
     *
     * @param null $logger
     * @param null $environment
     * @param null $customUrl
     *
     * @throws ConnectionException
     */
    public function __construct($logger = null, $environment = null, $customUrl = null, $debug = false)
    {

        if($logger instanceof Logger)
        {
            $this->logger = $logger;
        }
        $this->customUrl = $customUrl;
        $this->debug = $debug;

        $this->getUrlForEnvironment($environment);


    }

    /**
     * @param $environment
     * @throws ConnectionException
     */
    public function getUrlForEnvironment($environment)
    {
        $env = getenv ( self::FUEL_URL_ENV_NAME );

        if($env)
        {
            switch (strtoupper($env))
            {
                case self::FUEL_NAME_DEV:
                    $this->url = self::FUEL_URL_DEV;
                    break;
                case self::FUEL_NAME_TEST:
                    $this->url = self::FUEL_URL_TEST;
                    break;
                case self::FUEL_NAME_TESTINT:
                    $this->url = self::FUEL_URL_TESTINT;
                    break;
                case self::FUEL_NAME_TEST_1:
                    $this->url = self::FUEL_URL_TEST_1;
                    break;
                case self::FUEL_NAME_TEST_2:
                    $this->url = self::FUEL_URL_TEST_2;
                    break;
                case self::FUEL_NAME_TEST_3:
                    $this->url = self::FUEL_URL_TEST_3;
                    break;
                case self::FUEL_NAME_PROD:
                    $this->url = self::FUEL_URL_PROD;
                    break;
                case self::FUEL_NAME_STAGING:
                    $this->url = self::FUEL_URL_STAGING;
                    break;
                case self::FUEL_NAME_LOCAL:
                    $this->url = self::FUEL_URL_LOCAL;
                    break;
                case self::FUEL_NAME_CUSTOM:
                    if(empty($this->customUrl))
                    {
                        throw new ConnectionException($this, "Invalid value on env on CUSTOM url customUrl parameter its required ", 0);
                    }
                    $this->url = $this->customUrl;
                    break;
                default:
                    throw new ConnectionException($this, "Invalid value on env " . self::FUEL_URL_ENV_NAME, 0);
            }
        }else{
            switch (strtoupper($environment))
            {
                case self::FUEL_NAME_DEV:
                    $this->url = self::FUEL_URL_DEV;
                    break;
                case self::FUEL_NAME_TEST:
                    $this->url = self::FUEL_URL_TEST;
                    break;
                case self::FUEL_NAME_TESTINT:
                    $this->url = self::FUEL_URL_TESTINT;
                    break;
                case self::FUEL_NAME_TEST_1:
                    $this->url = self::FUEL_URL_TEST_1;
                    break;
                case self::FUEL_NAME_TEST_2:
                    $this->url = self::FUEL_URL_TEST_2;
                    break;
                case self::FUEL_NAME_TEST_3:
                    $this->url = self::FUEL_URL_TEST_3;
                    break;
                case self::FUEL_NAME_STAGING:
                    $this->url = self::FUEL_URL_STAGING;
                    break;
                case self::FUEL_NAME_LOCAL:
                    $this->url = self::FUEL_URL_LOCAL;
                    break;
                case self::FUEL_NAME_PROD:
                    $this->url = self::FUEL_URL_PROD;
                    break;
                case self::FUEL_NAME_CUSTOM:
                    if(empty($this->customUrl))
                    {
                        throw new ConnectionException($this, "Invalid value on env on CUSTOM url customUrl parameter its required ", 0);
                    }
                    $this->url = $this->customUrl;
                    break;
                default:
                    throw new ConnectionException($this, "Invalid value on env " . self::FUEL_URL_ENV_NAME, 0);
            }
        }
    }

    public function getCompleteUrl($path)
    {
        if($this->isDebugActive()) {
            if (strpos($path, '?') !== false) {
                $path .= '&';
            } else {
                $path .= '?';
            }

            $path .= 'modoApiDebug=1';
        }

        return $this->url . "/api/v1" . $path;
    }

    public function manageUrlQuerySymbols($completeUrl) {
        if (strpos($completeUrl, '?') !== false) {
            $completeUrl .= '&';
        } else {
            $completeUrl .= '?';
        }

        return $completeUrl;
    }

    public function saveResponse($responseRaw, $httpCode)
    {
        $response = new ResponseDTO();
        $response->fillWithBodyJson($responseRaw, $httpCode);
        $this->response = $response;
    }

    /**
     * @param $message
     * @param $level
     * @throws \Exception
     */
    public function writeLogException($message)
    {
        //$this->logger->error($message . " http: " . $this->httpcode . " response: " . $this->response . " request: " . json_encode($this->request));
        try{
            if($this->logger instanceof Logger)
            {
                $this->logger->log('error', $message . ' ' . $this->httpcode . " response: " . $this->response . " request: " . json_encode($this->request), array("url"=>"", "body"=>[]));
            }
        }catch(\Exception $e)
        {

        }

    }

    /**
     * @param $message
     */
    public function writeLog($message)
    {
        if($this->logger instanceof  Logger)
        {
            $this->logger->log('notice', $message);
        }
    }

    public function resetRequestAndResponse()
    {
        $this->request = null;
        $this->response = null;
    }

    public function setCredentials($curl)
    {
        return $curl;
    }

    #GET SERVICES
    /**
     * @param $path
     * @param  $queryItems
     * @throws ConnectionException
     */
    public function requestWilcardGet($path, $queryItems = [])
    {

        //Reset Request and Response
        $this->resetRequestAndResponse();
        if(!is_array($queryItems))
        {
            throw new ConnectionException($this, "Invalid 'queryItems' parameter must be an iterable with QueryItem instances");
        }

        $extraFilters = $this->getQueryParams($queryItems);
        if(!empty($extraFilters))
        {
            $path .= "?" . $extraFilters;
        }

        $completeUrl = $this->getCompleteUrl($path);
        $this->requestRawGet($completeUrl);
    }

    /**
     * @throws ConnectionException
     */
    public function requestNextPage()
    {
        if(!$this->response->getPagination()->getNextPage())
        {
            return false;
            //throw new ConnectionException($this, "There are no more grow pages to consult");
        }

        //list Item Cliente
        $completeUrl = $this->getLastUrlRequest();
        $currentPage = $this->response->getPagination()->getPage();

        if(strpos($completeUrl, "page=" . $currentPage) !== false)
        {
            $completeUrl = str_replace("page=". $this->getResponse()->getPagination()->getPage(), "page=" . $this->response->getPagination()->getNextPage() ,$completeUrl);
        }else{
            if(strpos($completeUrl, "?") === false)
            {
                $completeUrl .= "?";
            }
            elseif((strpos($completeUrl, "&") !== false && strpos($completeUrl, "&" . $currentPage)+1 < strlen
                    ($completeUrl)) || strpos($completeUrl, "&") === false)
            {
                $completeUrl .= "&";
            }
            $completeUrl .= "page=" . $this->response->getPagination()->getNextPage();
        }

        $this->requestRawGet($completeUrl);
        return true;
    }

    /**
     * @throws ConnectionException
     */
    public function requestPreviousPage()
    {
        if(!$this->response->getPagination()->getPrevPage())
        {
            return false;
            //throw new ConnectionException($this, "There are no more less pages to consult");
        }

        //list Item Cliente
        $completeUrl = $this->getLastUrlRequest();
        $currentPage = $this->response->getPagination()->getPage();
        if(strpos($completeUrl, "page=" . $currentPage) !== false)
        {
            $completeUrl = str_replace("page=". $this->getResponse()->getPagination()->getPage(), "page=" . $this->response->getPagination()->getPrevPage() ,$completeUrl);
        }else{
            if(strpos($completeUrl, "?") === false)
            {
                $completeUrl .= "?";
            }
            elseif(strpos($completeUrl, "&") !== false && strpos($completeUrl, "&" . $currentPage)+1 < strlen($completeUrl))
            {
                $completeUrl .= "&";
            }
            $completeUrl .= "page=" . $this->response->getPagination()->getPrevPage();
        }

        $this->requestRawGet($completeUrl);
        return true;
    }

    /**
     * @param $completeUrl
     * @throws ConnectionException
     */
    protected function requestRawGet($completeUrl)
    {
        $httpVerb = 'GET';
        try{
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $completeUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $httpVerb,
                CURLOPT_USERAGENT => self::USER_AGENT_NAME,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => ['X-Sdk-Version' => self::SDK_VERSION]
            ));

            $curl = $this->setCredentials($curl);
            $output = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $this->request = curl_getinfo($curl);
            $this->curlLog($this->request);
            curl_close($curl);

            $this->saveResponse($output, $httpcode);

            $this->httpcode = $httpcode;
            $this->lastUrlRequest = $completeUrl;

            if($httpcode >= 300)
            {
                $this->httpcode = $httpcode;
                throw new ConnectionException($this, "Invalid Http code response");
            }
            $this->addInfo('GET', $httpcode, $completeUrl, []);

        }catch(ConnectionException $e){
            throw $e;
        }catch(\Exception $e)
        {
            throw new ConnectionException($this, $e->getMessage());
        }
    }

    /**
     * @param $path
     * @param  $queryItems
     * @throws ConnectionException
     */
    public function requestWilcardDelete($path, $queryItems = [])
    {

        //Reset Request and Response
        $this->resetRequestAndResponse();

        if(!is_array($queryItems))
        {
            throw new ConnectionException($this, "Invalid 'queryItems' parameter must be an iterable with QueryItem instances");
        }

        $extraFilters = $this->getQueryParams($queryItems);
        if(!empty($extraFilters))
        {
            $path .= "?" . $extraFilters;
        }

        $completeUrl = $this->getCompleteUrl($path);
        $this->requestRawDelete($completeUrl);
    }

    /**
     * @param $completeUrl
     * @throws ConnectionException
     */
    protected function requestRawDelete($completeUrl)
    {
        $httpVerb = 'DELETE';

        try{
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $completeUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $httpVerb,
                CURLOPT_USERAGENT => self::USER_AGENT_NAME,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => ['X-Sdk-Version' => self::SDK_VERSION]
            ));

            $curl = $this->setCredentials($curl);
            $output = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $this->request = curl_getinfo($curl);
            $this->curlLog($this->request);
            curl_close($curl);

            $this->saveResponse($output, $httpcode);
            $this->httpcode = $httpcode;
            $this->lastUrlRequest = $completeUrl;

            if($httpcode >=300)
            {
                $this->httpcode = $httpcode;
                throw new ConnectionException($this, "Invalid Http code response");
            }
            $this->addInfo('DELETE', $httpcode, $completeUrl, []);

        }catch(ConnectionException $e){
            throw $e;
        }catch(\Exception $e)
        {
            throw new ConnectionException($this, $e->getMessage());
        }
    }

    #POST SERVICES
    /**
     * @param $path
     * @param  $queryItems
     * @throws ConnectionException
     */
    public function requestWilcardPost($path, $data, $queryItems = [])
    {

        //Reset Request and Response
        $this->resetRequestAndResponse();

        #QueryItems are allowed for styleResponse
        if(!is_array($queryItems))
        {
            throw new ConnectionException($this, "Invalid 'queryItems' parameter must be an iterable with QueryItem instances");
        }

        $extraFilters = $this->getQueryParams($queryItems);
        if(!empty($extraFilters))
        {
            $path .= "?" . $extraFilters;
        }

        $bodyJson = null;
        if(is_array($data))
        {
            $bodyJson = json_encode(array("data"=>$data));
        }

        $completeUrl = $this->getCompleteUrl($path);
        $this->requestRawPostPut($completeUrl,  $bodyJson);
    }

    #PUT SERVICES
    /**
     * @param $path
     * @param  $queryItems
     * @throws ConnectionException
     */
    public function requestWilcardPut($path, $data, $queryItems = [])
    {

        //Reset Request and Response
        $this->resetRequestAndResponse();

        #QueryItems are allowed for styleResponse

        if(!is_array($queryItems))
        {
            throw new ConnectionException($this, "Invalid 'queryItems' parameter must be an iterable with QueryItem instances");
        }

        $extraFilters = $this->getQueryParams($queryItems);
        if(!empty($extraFilters))
        {
            $path .= "?" . $extraFilters;
        }

        if(is_array($data))
        {
            $bodyJson = json_encode(array("data"=>$data));
        } else {
            $bodyJson = $data;
        }

        $completeUrl = $this->getCompleteUrl($path);
        $this->requestRawPostPut($completeUrl,  $bodyJson, 'PUT');
    }
    /**
     * @param $completeUrl
     * @param string $httpVerb
     * @throws ConnectionException
     */
    protected function  requestRawPostPut($completeUrl, $body,  $httpVerb='POST', $afterRefresh = false)
    {

        try{
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $completeUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => strtoupper($httpVerb),
                CURLOPT_USERAGENT => self::USER_AGENT_NAME,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => ['X-Sdk-Version' => self::SDK_VERSION]
            ));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body);

            $curl = $this->setCredentials($curl);
            $output = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $this->request = curl_getinfo($curl);
            $this->curlLog($this->request);
            curl_close($curl);

            $this->saveResponse($output, $httpcode);
            $this->httpcode = $httpcode;
            $this->lastUrlRequest = $completeUrl;

            $output = json_decode($output, true);
            if($httpcode == 401 && !$afterRefresh)
            {
                $outputCode = $output['codeResponse'];
                $this->httpcode = $httpcode;
                $this->addInfo($httpVerb, $httpcode, $completeUrl, $body);

                switch ($outputCode) {
                    case ConnectionCodes::RESPONSE_UNATHORIZED_REFRESH:
                    {
                        if ($this->refreshToken()) {
                            $this->requestRawPostPut($completeUrl, $body,  $httpVerb, true);
                        };

                        $this->addInfo($httpVerb, $httpcode, $completeUrl, $body);
                        break;
                    }
                    case 110: {
                        throw new ConnectionException($this, "Token expired. License Expired");
                    }
                    case 130: {
                        if( $this instanceof ConnectionB2b )
                        {
                            $status = $this->login();
                            if($status === true)
                            {
                                $this->requestRawPostPut($completeUrl, $body,  $httpVerb, true);
                            }
                        }
                        throw new ConnectionException($this, "Token expired");
                    }

                    default:

                        throw new ConnectionException($this, "Invalid Http code response");
                }
            }

            if($httpcode >= 300)
            {
                $this->httpcode = $httpcode;
                throw new ConnectionException($this, "Invalid Http code response");
            }
            $this->addInfo($httpVerb, $httpcode, $completeUrl, $body);
        }catch(ConnectionException $e){
            throw $e;
        }catch(\Exception $e)
        {
            throw new ConnectionException($this, $e->getMessage());
        }
    }

    private function addInfo($httpVerb, $httpcode, $completeUrl, $body)
    {
        try{
            if($this->logger instanceof Logger)
            {
                $this->logger->log('info','Request ' . $httpVerb . ' ' . $httpcode . " to: " .$completeUrl, array("url"=>$completeUrl, "body"=>$body));
            }
        }catch(\Exception $e)
        {

        }
    }

    public function curlLog($contentCurl)
    {
        if(!$this->isDebugActive()) {
            return;
        }

        try{
            if($this->logger instanceof Logger)
            {
                $this->logger->log('info', "CURLDATA: ".json_encode($contentCurl, JSON_UNESCAPED_SLASHES));
            }
        }catch(\Exception $e)
        {

        }
    }


    private function getQueryParams($queryItems)
    {
        $query = "";
        $firstQuery = true;

        /** @var QueryItem $queryItem */
        foreach ($queryItems as $queryItem)
        {
            $aux = $queryItem->composeQuery();
            if($aux)
            {
                if(!$firstQuery)
                {
                    $query .= "&";
                }else{
                    $firstQuery = false;
                }

                $query .= $aux;

            }
        }

        return $query;
    }

    /**
     * @return ResponseDTO
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return mixed
     */
    public function getHttpcode()
    {
        return $this->httpcode;
    }

    /**
     * @return mixed
     */
    public function getFechaRefrescoLicencia()
    {
        return $this->fechaRefrescoLicencia;
    }

    /**
     * @return mixed
     */
    public function getLastUrlRequest()
    {
        return $this->lastUrlRequest;
    }

    public function getErrorToShow()
    {
        return $this->errorToShow;
    }

    public function setErrorToShow( $errorToShow )
    {
        $this->errorToShow = $errorToShow;
    }

    public function returnErrorToShow()
    {
        if ($this->getHttpcode() && $this->getHttpcode() == 200)
        {
            return null;
        }

        $response = json_decode($this->getResponse(), true);

        if(is_array($response) && array_key_exists('errorToShow', $response))
        {
            $this->setErrorToShow($response[self::ERROR_TRANSLATED_RESPONSE_VARNAME]);
        }

        return $this->getErrorToShow();
    }

    public function isDebugActive() {
        return $this->debug === true;
    }

    public function enableDebug() {
        return $this->debug = true;
    }

    public function disableDebug() {
        return $this->debug = false;
    }

}
