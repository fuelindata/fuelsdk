<?php


namespace FuelSdk\Utils;


use FuelSdk\Exception\ConnectionException;

class QueryParam extends QueryRelation
{
    const ARRAY_NAME_URLENCODED = ["tipoConcatenadoNoOr","filtrarFuelQuery"];

    protected $name;

    public function __construct($name, $relation, $value, $not=false)
    {
        if( empty($name))
        {
            throw  new ConnectionException("The 'name' parameter must not be empty.");
        }

        parent::__construct($relation, $value, $not);

        $this->name = $name;
    }

    public function composeQuery()
    {
        if(in_array($this->name, self::ARRAY_NAME_URLENCODED))
        {
            if(!is_array($this->value))
            {
                return "" . $this->name . $this->getSubFixName() . $this->notSufix . "=" . urlencode($this->value);
//                return "" . $this->name . $this->getSubFixName() . "=" . $this->value;
            }else{
                $keyValue = [];
                foreach ($this->value  as $keyItem=>$valueItem)
                {
                    if(array_key_exists($keyItem, $keyValue))
                    {
                        if(!is_array($keyValue[$keyItem]))
                        {
                            $keyValue[$keyItem] = [$keyValue[$keyItem], $valueItem];
                        }else{
                            $keyValue[$keyItem][] = $valueItem;
                        }
                    }else{
                        $keyValue[$keyItem] = $valueItem;
                    }
                }

                $response = "";
                $first = true;
                foreach($keyValue as $finalKey => $finalValue)
                {


                    if(!is_array($finalValue) || count($finalValue) === 1)
                    {
                        if(!$first)
                        {
                            $response .= "&";
                        }
                        $response .= "" . $finalKey . $this->notSufix . "=" . $finalValue;
                        $first=false;
                    }else{
                        $firstAux=true;
                        foreach ($finalValue  as $finalValueItem)
                        {
                            if(!$firstAux)
                            {
                                $response .= "&";
                            }
                            $response .= "" . $finalKey . $this->notSufix . "[]=" . $finalValueItem;
                        }
                    }
                }
            }
            return "" . $this->name . "=" . urlencode($response);
        }else{
            $response = "";

            if(!is_array($this->value))
            {
//                if(is_string($this->value) && strpos($this->value, "=") !== false)
//                {
//                    return "" . $this->name . $this->getSubFixName() . "=" . urlencode($this->value);
//                }
                $equalValue = ($this->relation === 5)?"":("=" . $this->value);
                return "" . $this->name . $this->getSubFixName() . $this->notSufix . $equalValue;
            }elseif( count($this->value) === 2 && $this->relation === QueryRelation::FUEL_INT_QUERY_RELATION_BETWEEN) {
                return "" . $this->name . $this->getSubFixName() . "=" . $this->value[0] . "___BETWEEN___" . $this->value[1];
            }elseif( count($this->value) === 1){
//                if(is_string($this->value[0]) && strpos($this->value[0], "=") !== false)
//                {
//                    return "" . $this->name . $this->getSubFixName() . "=" . urlencode($this->value[0]);
//                }
                $equalValue = ($this->relation === 5)?"":("=" . $this->value[0]);
                return "" . $this->name . $this->getSubFixName() . $this->notSufix . $equalValue;
            }else{
                foreach ($this->value as $value)
                {
                    //Not allowed isNull on array with values.
                    if(!empty($response))
                    {
                        $response .= "&";
                    }
                    //Nota: No se contempla que los parametros de listas/Array puedan tener urlencode
                    $response .= $this->name . $this->getSubFixName() . $this->notSufix . "[]=" . $value;
                }
            }
        }


        return $response;
    }


}
