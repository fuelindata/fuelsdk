<?php


namespace FuelSdk\Utils;


use FuelSdk\Exception\ConnectionException;

abstract class QueryRelation extends QueryValue
{

    const FUEL_INT_QUERY_RELATION_EQUAL = 1;
    const FUEL_INT_QUERY_RELATION_GROW = 2;
    const FUEL_INT_QUERY_RELATION_LESS = 3;
    const FUEL_INT_QUERY_RELATION_LIKE = 4;
    const FUEL_INT_QUERY_RELATION_IS_NULL = 5;
    const FUEL_INT_QUERY_RELATION_BETWEEN = 6;

    const FUEL_STR_NOT_QUERY_PARAM = "NOT";

    /**
     * Availables relation
     * 1 = EQUAL
     * 2 = GROW
     * 3 = LESS
     * 4 = CONTAINS/LIKE
     * 5 = IS NULL
     */
    protected $relation;

    protected $notSufix = "";

    public function __construct($relation, $value, $not = false)
    {
        if( empty($relation))
        {
            throw  new ConnectionException("The 'relation' parameter must not be empty.");
        }

        $relation = (int)$relation;

        if(!in_array($relation, $this->availablesRelations(), true))
        {
            throw  new ConnectionException("Invalid value for 'relation' parameter.");
        }

        parent::__construct($value);

        $this->relation = $relation;
        $this->notSufix = $not?self::FUEL_STR_NOT_QUERY_PARAM:"";
    }

    public function availablesRelations()
    {
        return array(
            self::FUEL_INT_QUERY_RELATION_EQUAL,
            self::FUEL_INT_QUERY_RELATION_GROW,
            self::FUEL_INT_QUERY_RELATION_LESS,
            self::FUEL_INT_QUERY_RELATION_LIKE,
            self::FUEL_INT_QUERY_RELATION_IS_NULL
        );
    }

    public function getSubFixName()
    {
        switch ($this->relation)
        {
            case self::FUEL_INT_QUERY_RELATION_EQUAL:
                $key =  "";
                break;
            case self::FUEL_INT_QUERY_RELATION_GROW:
                $key = "Grow";
                break;
            case self::FUEL_INT_QUERY_RELATION_LESS:
                $key =  "Less";
                break;
            case self::FUEL_INT_QUERY_RELATION_LIKE:
                $key =  "Like";
                break;
            case self::FUEL_INT_QUERY_RELATION_IS_NULL:
                $key = "IsNull";
                break;
            default:
                $key = "";
        }

        return $key;
    }


}