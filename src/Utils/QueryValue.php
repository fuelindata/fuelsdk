<?php


namespace FuelSdk\Utils;


use FuelSdk\Exception\ConnectionException;

abstract class QueryValue implements QueryItem
{
    protected $value;

    /**
     * QueryValue constructor.
     * @param $value
     */
    public function __construct( $value)
    {
        $this->value = $value;
    }
}