<?php


namespace FuelSdk;

use Exception;
use FuelSdk\Exception\ConnectionException;

class ConnectionB2b extends Connection
{
    //Params required for login with key and pass;
    private $pass;
    private $key;
    private $userId;
    private $userIdOld;
    public $token;
    public $refreshToken;
    public $licenseId;
    public $sdkVersion;

    /**
     * ConnectionKeyPass constructor.
     * @param $pass
     * @param $key
     * @param $logger
     * @param $environment
     * @param null $customUrl
     * @throws Exception|ConnectionException
     */
    public function __construct($pass, $key, $logger, $environment, $customUrl = null, $debug = false)
    {
        if (empty($key)) {
            throw  new ConnectionException("The 'apikey' parameter must not be empty.");
        }
        if (empty($pass)) {
            throw  new ConnectionException("The 'pass' parameter must not be empty.");
        }

        parent::__construct($logger, $environment, $customUrl, $debug);

        $this->pass = $pass;
        $this->key = $key;

        $this->login();
        $this->writeLog('Login with apikey and pass is completed successfully. apikey: ' . $this->key);
    }

    /**
     * @throws ConnectionException
     */
    public function login()
    {

        $completeUrl = $this->getCompleteUrl(WebService::PATH_AUTH_LOGIN_B2B);

        try {
            if(true) {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $completeUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array('key' => $this->key, 'pass' => $this->pass),
                    CURLOPT_USERAGENT => self::USER_AGENT_NAME,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_HTTPHEADER => ['X-Sdk-Version' => self::SDK_VERSION]
                ));

                $output = curl_exec($curl);
                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $response = json_decode($output, true);
                $this->request = curl_getinfo($curl);
                $this->curlLog($this->request);

//                $this->request = curl_getinfo($curl);
                curl_close($curl);

                $this->saveResponse($output, $httpcode);
                $this->httpcode = $httpcode;

                if($this->returnErrorToShow()) {
                    return $this->returnErrorToShow();
                }
            }

            if (isset($response["data"]["jwt"])) {
                $this->setToken($response["data"]["jwt"]);
            }

            if (isset($response["data"]["refresh"])) {
                $this->setRefreshToken($response["data"]["refresh"]);
            }

            if (isset($response["data"]["fechaRefrescoLicencia"])) {
                $this->fechaRefrescoLicencia = new \DateTime($response["data"]["fechaRefrescoLicencia"]);
            }
        } catch (ConnectionException $e) {
            return $e;
        } catch (\Exception $e) {
            throw new ConnectionException($this, $e->getMessage());
        }

        return true;
    }

    /**
     * @throws ConnectionException
     */
    public function loginUser()
    {
        try{
            if(!is_null($this->userId))
            {
                //Get Usuario
                $completeUrl = $this->getCompleteUrl(WebService::PATH_USER_GET);
                $completeUrl = $this->manageUrlQuerySymbols($completeUrl);
                $completeUrl .= "id=$this->userId";
                $httpVerb = 'GET';
            }elseif(!is_null($this->userIdOld)) {
                $completeUrl = $this->getCompleteUrl(WebService::PATH_USER_GET);
                $completeUrl = $this->manageUrlQuerySymbols($completeUrl);
                $completeUrl .= "idOld=$this->userIdOld";
                $httpVerb = 'GET';
            }else {
                throw new ConnectionException($this, "UserId is required");
            }

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $completeUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => $httpVerb,
                CURLOPT_USERAGENT => self::USER_AGENT_NAME,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTPHEADER => ['X-Sdk-Version' => self::SDK_VERSION]
            ));

            $curl = $this->setCredentials($curl);
            $output = curl_exec($curl);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $this->request = curl_getinfo($curl);
            $this->curlLog($this->request);
            curl_close($curl);

            $this->saveResponse($output, $httpCode);
            $this->httpcode = $httpCode;

            if($this->returnErrorToShow())
            {
                return $this->returnErrorToShow();
            }

            if(!is_null($this->userIdOld) && isset($this->response->getData()["id"]))
            {
                $this->userId = $this->response->getData()["id"];
                $this->writeLog("Set Id User to: " . $this->userId . " based on Id old: " . $this->userIdOld);
                $this->userIdOld = null;
            }

        }catch(ConnectionException $e){
            return $e;
        }catch(\Exception $e)
        {
            throw new ConnectionException($this, $e->getMessage());
        }

        return true;
    }

    public function setCredentials($curl)
    {
        if(!is_null($this->userId))
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'cliente: ' . $this->licenseId,
                'usuario: ' . $this->userId,
                "Authorization: Bearer " . $this->token . " ",
                'Content-Type: application/json',
                'X-Sdk-Version: '.self::SDK_VERSION
            ));
        }elseif(!is_null($this->userIdOld)){
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'cliente: ' . $this->licenseId,
                'usuarioold: ' . $this->userIdOld,
                "Authorization: Bearer " . $this->token . " ",
                'Content-Type: application/json',
                'X-Sdk-Version: '.self::SDK_VERSION
            ));
        }else{
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'cliente: ' . $this->licenseId,
                "Authorization: Bearer " . $this->token . " ",
                'Content-Type: application/json',
                'X-Sdk-Version: '.self::SDK_VERSION
            ));
        }

        return $curl;
    }

    /**
     * @param $userId
     * @throws ConnectionException
     */
    public function setUserId($userId)
    {
        $this->userIdOld = null;
        $this->userId = $userId;
        //Se comenta el login de usuario para reducir el tiempo de carga del sdk
        /*$status = $this->loginUser();
        if($status === true)
        {
            $this->writeLog('User with License and IP authentication completes successfully. License: ' . $this->licenseId . ' User: ' .$this->userId);
        }
        */
    }

    /**
     * @param $userIdOld
     * @throws ConnectionException
     */
    public function setUserIdOld($userIdOld)
    {
        $this->userId = null;
        $this->userIdOld = $userIdOld;
        //Se comenta el login de usuario para reducir el tiempo de carga del sdk
        /*$status = $this->loginUser();
        if($status === true)
        {
            $this->writeLog('User with License and IP authentication completes successfully. License: ' . $this->licenseId . ' User: ' .$this->userId . ' UserOld: ' . $this->userIdOld );
        }
        */
    }

    /**
     * @param mixed $licenseId
     */
    public function setLicenseId($licenseId)
    {
        $this->licenseId = $licenseId;
    }

    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    protected function refreshToken()
    {
        try {
            $this->requestWilcardPost(WebService::PATH_AUTH_REFRESH_B2B, [
                "refresh" => $this->getRefreshToken(),
                'Authorization' => $this->getToken()
            ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

}