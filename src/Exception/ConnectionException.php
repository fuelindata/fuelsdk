<?php

namespace FuelSdk\Exception;

use Exception;
use FuelSdk\Connection;

class ConnectionException extends Exception
{
    /**
     * @throws Exception
     */
    public function __construct($connection, $message = "", $code = 0, $previous = null)
    {
        if($connection instanceof Connection)
        {
            $connection->writeLogException($message);
        }
        parent::__construct($message, $code, $previous);
    }
}